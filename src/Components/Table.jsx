/* 
  This component maps through the EmployeeData array and renders
  the SingleEmployee component for each element in the array
*/

import React from "react";

import classes from "./Table.module.css";
import SingleEmployee from "./Employees/SingleEmployee";

const Table = ({ employeeData, removeEmployee }) => {
  // Passes currentEmployee from SingleEmployee up to App
  const removeEmployeeHandler = (currentEmployeeId) => {
    removeEmployee(currentEmployeeId);
  };

  console.log("names:", employeeData);
  return (
    <section>
      <table>
        {
          // conditionally render table head if employees >= 1
          employeeData.length >= 1 && (
            <thead>
              <tr>
                <th>{/* Empty heading above icons */}</th>
                <th>Name</th>
                <th>{/* Empty heading above remove buttons */}</th>
              </tr>
            </thead>
          )
        }
        <tbody>
          {
            // conditionally render add employee prompt all employees are removed
            employeeData.length === 0 && (
              <tr>
                <td className={classes.prompt}>Please add an employee</td>
              </tr>
            )
          }

          {employeeData
            .sort((a, b) => a.name.localeCompare(b.name))
            .map((employee) => {
              let randomNumber = Math.floor(Math.random() * 10000 + 1);

              return (
                <SingleEmployee
                  key={randomNumber}
                  employee={employee}
                  onRemoveEmployee={removeEmployeeHandler}
                />
              );
            })}
        </tbody>
      </table>
    </section>
  );
};

export default Table;
