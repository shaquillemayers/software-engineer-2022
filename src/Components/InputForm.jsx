import React, { useState, useRef } from "react";
import classes from "./InputForm.module.css";

const InputForm = (props) => {
  // Error state
  const [error, setError] = useState(false);

  // Grab input refs
  const nameInputRef = useRef();

  const submitHandler = (e) => {
    e.preventDefault();
    const enteredEmployeeName = nameInputRef.current.value;

    // VALIDATION

    // Regular Expression that allows for spaces and hyphens
    var regEx = /^[-\sa-zA-Z]+$/; // /^[a-zA-Z ]*$/;

    /* 
      NTS: Change below if to switch, then change error message on each case before submission
    */

    // Error first error-handling, checking that entered name:
    // 1. Isn't an empty string or a string with less than 2 characters.
    // 2. Meets the matches the regEx.
    // 3. Doesn't start or end with a hypen

    if (
      enteredEmployeeName.trim() === "" ||
      enteredEmployeeName.trim().length < 2 ||
      !enteredEmployeeName.match(regEx) ||
      enteredEmployeeName.charAt(0) === "-" ||
      enteredEmployeeName.charAt(enteredEmployeeName.length - 1) === "-"
    ) {
      setError("Employee name invalid.");
      return console.log("error");
    } else {
      setError(false);
      console.log(enteredEmployeeName);

      // Ensures entered data is handled correctly, ready to be sorted
      let validEmployeeName =
        enteredEmployeeName.trim().charAt(0).toUpperCase() +
        enteredEmployeeName.trim().slice(1).toLowerCase();

      let validEmployeeObj = {
        id: Math.floor(Math.random() * 10000 + 1),
        name: validEmployeeName,
      };

      // Pass validEmployeeObj to App
      props.onAddEmployee(validEmployeeObj);

      // Clear input after submission
      nameInputRef.current.value = "";
    }
  };

  return (
    <section>
      <form onSubmit={submitHandler}>
        <div className={classes.form}>
          <label htmlFor="name">Employee Name</label>
          <input ref={nameInputRef} type="text" id="name" required />
          <p>{!error ? "Please enter Employee details" : error}</p>
        </div>
        <div className={classes.actions}>
          <button>Add Employee</button>
        </div>
      </form>
    </section>
  );
};

export default InputForm;
