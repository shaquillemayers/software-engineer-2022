/* 
  This component renders the table row for each employee in the Table
*/

import userIcon from "../../assets/user-icon.png";
import classes from "./SingleEmployee.module.css";

const SingleEmployee = ({ employee, onRemoveEmployee }) => {
  let { id: employeeId, name: employeeName } = employee;

  // remove handler passes the current employee to
  const removeEmployeeHandler = () => {
    onRemoveEmployee(employeeId);
  };

  // Ensure that employeeName is a case insenstive string
  if (typeof employeeName === "string") {
    // if employeeName is a string, capitalise first letter ONLY
    var validEmployee =
      employeeName.trim().charAt(0).toUpperCase() +
      employeeName.trim().slice(1).toLowerCase();
  }

  return (
    <>
      {validEmployee && (
        <tr>
          <td className="table">
            <img alt="user icon" src={userIcon} />
          </td>
          <td className="table">{employeeName.trim()}</td>
          <td className={classes["remove-button-container"]}>
            <button className={classes.remove} onClick={removeEmployeeHandler}>
              Remove
            </button>
          </td>
        </tr>
      )}
    </>
  );
};

export default SingleEmployee;
