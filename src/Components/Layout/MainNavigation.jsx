/* 
  Header and Navigation bar
*/

/* import { NavLink } from "react-router-dom"; */
import React from "react";

import classes from "./MainNavigation.module.css";

const MainNavigation = () => {
  // Main navigations header
  return (
    <header className={classes.header}>
      <a href="/">
        <div className={classes.logo}></div>
      </a>
      <nav>
        <ul>
          <li>
            <a href="/" className={classes["header-link"]}>
              Employees
            </a>
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default MainNavigation;
