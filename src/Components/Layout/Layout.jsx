import React, { Fragment } from "react";
import MainNavigation from "./MainNavigation";

const Layout = (props) => {
  // Layout wrapper that displays main navigation as well as all of the components it wraps around.
  return (
    <Fragment>
      <MainNavigation />
      <main>{props.children}</main>
    </Fragment>
  );
};

export default Layout;
