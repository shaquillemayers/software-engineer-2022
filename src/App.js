/* 
  This component renders the h1 element and the inputForm & Table components
*/

import React, { useState } from "react";

import "./App.css";

import Layout from "./Components/Layout/Layout";
import InputForm from "./Components/InputForm";
import Table from "./Components/Table";

const App = () => {
  // swapped "names list" for employeeData, in case I use objects for advanced tasks
  // let initialEmployees = ["Mark", "Rose", "Shaquille"];

  let initialEmployees = [
    { name: "Mark", id: 1 },
    { name: "Rose", id: 2 },
    { name: "Shaquille", id: 3 },
  ];

  // Sort initial employees alphabetically by name
  const sortedInitialEmployees = initialEmployees.sort((a, b) =>
    a.name.localeCompare(b.name)
  );

  // set initial employeeData state. Later passed to Table component
  const [employeeData, setEmployeeData] = useState(sortedInitialEmployees);

  const removeEmployeeHandler = (currentEmployeeId) => {
    // receives the current employees Id from Table component and removes from EmployeeData array
    setEmployeeData((prevState) =>
      prevState.filter((employee) => employee.id !== currentEmployeeId)
    );

    console.log(`Employee removed Id: ${currentEmployeeId}`);
  };

  const handleSubmitClick = (employeeObj) => {
    // adds entered employee object to the EmployeeData array
    setEmployeeData((prevState) => [...prevState, employeeObj]);
  };

  return (
    <Layout>
      <h1>Employee Database</h1>
      <InputForm onAddEmployee={handleSubmitClick} />
      <Table
        names={employeeData}
        employeeData={employeeData}
        removeEmployee={removeEmployeeHandler}
      />
    </Layout>
  );
};

export default App;
